package ru.eltech.crimemanager.presentation

import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TimePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ru.eltech.crimemanager.databinding.FragmentCrimeBinding
import java.sql.Date

class CrimeFragment : Fragment() {

    private var _binding: FragmentCrimeBinding? = null
    private val binding: FragmentCrimeBinding
        get() = _binding ?: throw RuntimeException("FragmentCrimeBinding == null")

    private var date: String? = null
    private var time: String? = null

    private val viewModel by lazy {
        ViewModelProvider(this)[CrimeViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCrimeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setupButtons()
    }

    private fun setupButtons() {
        setupTimeBtn()
        setupDateBtn()
        setupTitle()

        binding.btnSave.setOnClickListener {
            viewModel.saveData(
                binding.etTitle.text.toString(),
                time,
                date,
                binding.cbIsSolved.isChecked
            )
        }

        viewModel.result.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), "$it", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setupTitle() {
        viewModel.emptyTitle.observe(viewLifecycleOwner) {
            binding.etTitle.error = "Empty Title"
        }
        viewModel.emptyTitle.observe(viewLifecycleOwner) {
            binding.etTitle.error = if (it) "Empty Title" else null
        }
        binding.etTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.resetTitleError()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })
    }

    private fun setupDateBtn() {
        binding.setDate.setOnClickListener {
            viewModel.resetDateError()
            val datePickerFragment = DatePickerFragment()
            val supportFragmentManager = requireActivity().supportFragmentManager

            supportFragmentManager.setFragmentResultListener(
                DatePickerFragment.REQUEST_DATE,
                viewLifecycleOwner
            ) { resultKey, bundle ->
                if (resultKey == DatePickerFragment.REQUEST_DATE) {
                    date = bundle.getString(DatePickerFragment.SELECTED_DATE)
                    binding.setDate.text = date
                }
            }

            datePickerFragment.show(supportFragmentManager, "DatePickerFragment")
        }

        viewModel.emptyDate.observe(viewLifecycleOwner) {
            binding.setDate.error = if (it) "Empty Date" else null
        }
    }

    private fun setupTimeBtn() {
        binding.btnSetTime.setOnClickListener {
            viewModel.resetTimeError()
            val timePickerFragment = TimePickerFragment()
            val supportFragmentManager = requireActivity().supportFragmentManager

            supportFragmentManager.setFragmentResultListener(
                TimePickerFragment.RC,
                viewLifecycleOwner
            ) { resultKey, bundle ->
                if (resultKey == TimePickerFragment.RC) {
                    time = bundle.getString(TimePickerFragment.SELECTED_TIME)
                    binding.btnSetTime.text = time
                }
            }

            timePickerFragment.show(supportFragmentManager, "TimePickerFragment")
        }
        viewModel.emptyTime.observe(viewLifecycleOwner) {
            binding.btnSetTime.error = if (it) "Empty Time" else null
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}