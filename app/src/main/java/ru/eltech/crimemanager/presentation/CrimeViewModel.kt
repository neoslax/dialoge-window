package ru.eltech.crimemanager.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.eltech.crimemanager.data.Crime

class CrimeViewModel : ViewModel() {

    private val _emptyTitle = MutableLiveData<Boolean>()
    val emptyTitle: LiveData<Boolean>
        get() = _emptyTitle

    private val _emptyDate = MutableLiveData<Boolean>()
    val emptyDate: LiveData<Boolean>
        get() = _emptyDate

    private val _emptyTime = MutableLiveData<Boolean>()
    val emptyTime: LiveData<Boolean>
        get() = _emptyTime

    private val _result = MutableLiveData<Crime>()
    val result: LiveData<Crime>
        get() = _result

    fun saveData(title: String, time: String?, date: String?, isSolved: Boolean) {
        var isCorrect = true
        when {
            time == null -> {
                _emptyTime.value = true
                isCorrect = false
            }
            date == null -> {
                _emptyDate.value = true
                isCorrect = false
            }
            title.isEmpty() -> {
                _emptyTitle.value = true
                isCorrect = false
            }


        }
        if (isCorrect && time != null && date != null) {
            _result.value = Crime(
                0, title, "$date : $time", isSolved
            )
        }

    }

    fun resetTimeError() {
        _emptyTime.value = false
    }

    fun resetDateError() {
        _emptyDate.value = false
    }

    fun resetTitleError() {
        _emptyTitle.value = false
    }
}