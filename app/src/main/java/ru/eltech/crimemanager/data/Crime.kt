package ru.eltech.crimemanager.data

data class Crime(
    val id: Int,
    val title: String,
    val date: String,
    val isSolved: Boolean
)